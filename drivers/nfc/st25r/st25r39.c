// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * Driver for ST ST25R SPI NFC Chip - core functions
 *
 * Copyright (C) 2020 Olliver Schinagl <oliver@schinagl.nl>
 * Copyright (c) 2020 ONPLICK, Paweł Żabiełowicz <pzabielowicz@onplick.com>
 * Copyright (C) 2020 Yaroslav Palamar <yaroslavpalamar@gmail.com>
 */

#include <linux/device.h>
#include <linux/errno.h>
#include <linux/interrupt.h>
#include <linux/jiffies.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/netdevice.h>
#include <linux/nfc.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/property.h>
#include <linux/regulator/consumer.h>
#include <linux/skbuff.h>
#include <linux/slab.h>
#include <linux/spi/spi.h>
#include <linux/timer.h>
#include <linux/workqueue.h>
#include <net/nfc/digital.h>
#include <net/nfc/nfc.h>

#include "st25r39_reg.h"

#define ST25R39_VERSION "0.1"
#define ST25R39_NAME "st25r39"
#define ST25R39_DESC "STMicroelectronics st25r39 NFC driver"

#define ST25R39_SUPPORTED_PROT	NFC_PROTO_ISO14443_MASK
#define ST25R39_CAPABILITIES	NFC_DIGITAL_DRV_CAPS_IN_CRC

#define ST25R39_CLOCK_FREQUENCY_13MHz 13560000
#define ST25R39_CLOCK_FREQUENCY_27MHz 27120000

#define ST25R39_CMD_LEN 1

#define ST25R39_FIFO_DEPTH_MIN 4
#define ST25R39_FIFO_DEPTH_MAX 96

/* 1 byte for fifo load command */
#define ST25R39_HEADROOM_LEN 1
#define ST25R39_TAILROOM_LEN 0

/*
 * ISO/IEC 14443A specification command that activiates communication with a tag
 */
#define ST25R39_REQA 0x26

#define ceiling_fraction(numerator, divider) \
	(((numerator) + ((divider) - 1)) / (divider))

/*
 * Delta Time for polling during Activation (ATS) : 16.4ms
 * Digital 1.1 13.8.1.1 & A.6
 */
#define ST25R39_ISODEP_T4T_DTIME_POLL_11 216960

#define ST25R39_ISODEP_FWT_ACTIVATION_TIME 71680
/*
 * Activation frame waiting time FWT(act) 71680/fc (~5286us)
 * Digital 1.1 13.8.1.1 & A.6
 */
#define ST25R39_ISODEP_T4T_FWT_ACTIVATION \
	(ST25R39_ISODEP_FWT_ACTIVATION_TIME + ST25R39_ISODEP_T4T_DTIME_POLL_11)

#define ST25R39_NFCA_MIN_LISTEN_FDT 1236

/*
 * NFC-A min FDT for rx is 1236 (Digital 1.1  6.10.1) when last bit is zero
 * Adding 3 * 128 for multiple cards where response may take longer (JCOP cards)
 *
 */
#define ST25R39_NFCA_LISTEN_FDT		(ST25R39_NFCA_MIN_LISTEN_FDT + 3 * 128)
#define ST25R39_NFCA_FWT_A_ADJUSTMENT		512
#define ST25R39_NFCA_POLL_FTD_ADJUSTMENT	276
#define ST25R39_FDT_ADJUST			64
#define ST25R39_1FC_IN_64FC			64
#define ST29R39_1FC_IN_4096FC			4096
#define ST25R39_FC_TO_64FC(val)   ceiling_fraction((val), ST25R39_1FC_IN_64FC)
#define ST25R39_FC_TO_4096FC(val) ceiling_fraction((val), ST29R39_1FC_IN_4096FC)

/*
 * Timeout in milliseconds to be used on a potential
 * missing RXE - Silicon ST25R3911B Errata #1.1
 */
#define ST25R39_NO_RXE_TIMEOUT			10

struct st25r39_config {
	bool has_aat;
};

struct st25r39_data {
	struct nfc_digital_dev *nfc_ddev;
	struct spi_device *spi;
	struct device *dev;

	struct regulator *supply_vdd;
	struct regulator *supply_vio;

	uint32_t clk_freq;

	const struct st25r39_config *config;

	uint8_t rf_tech;

	/* Response callback after in_send_cmd sent request */
	nfc_digital_cmd_complete_t complete_cb;
	void *cb_arg;
	struct sk_buff *skb_resp;

	struct mutex lock;
	struct semaphore exchange_lock;

	int framing;
	bool rxs_signaled;
	struct delayed_work timeout_work;
};

static struct st25r39_config st25r3910_config = {
	.has_aat = true,
};

static struct st25r39_config st25r3911b_config = {
	.has_aat = true,
};

static struct st25r39_config st25r3912_config = {
	.has_aat = false,
};

static struct st25r39_config st25r3913_config = {
	.has_aat = true,
};

static struct st25r39_config st25r3914_config = {
	.has_aat = true,
};

static struct st25r39_config st25r3915_config = {
	.has_aat = false,
};

static struct st25r39_config st25r3916_config = {
	.has_aat = true,
};

static struct st25r39_config st25r3917_config = {
	.has_aat = false,
};

static void _digital_layer_send_error(struct st25r39_data *drvdata)
{
	drvdata->skb_resp = ERR_PTR(-ENODATA);
	drvdata->complete_cb(drvdata->nfc_ddev, drvdata->cb_arg, drvdata->skb_resp);
	drvdata->skb_resp = NULL;
	up(&drvdata->exchange_lock);
}

static int st25r39_cmd(struct st25r39_data *drvdata, uint8_t cmd)
{
	/* Command codes are pre-converted defined to match the datasheet */
	return spi_write(drvdata->spi, &cmd, sizeof(cmd));
}

static int st25r39_spi_xfer(struct st25r39_data *drvdata, void *buf, unsigned len)
{
	struct spi_transfer xfers[] = {
		{
			.rx_buf = buf,
			.tx_buf = buf,
			.len = len,
		},
	};
	int ret;

	if (!drvdata)
		return -ENODEV;

	if (!drvdata->spi)
		return -ENODEV;

	if ((!buf) || (len < ST25R39_CMD_LEN) || (len > ST25R39_FIFO_DEPTH_MAX))
		return -EINVAL;

	ret = spi_sync_transfer(drvdata->spi, xfers, ARRAY_SIZE(xfers));
	if (ret)
		return ret;

	return 0;
}

static int st25r39_spi_fifo_read(struct st25r39_data *drvdata, void *buf, unsigned buf_size)
{
	uint8_t mode = ST25R39_MODE_FIFO_READ;

	return spi_write_then_read(drvdata->spi, &mode, sizeof(mode), buf, buf_size);
}

static int st25r39_read(struct st25r39_data *drvdata, uint8_t reg, uint8_t *val)
{
	uint8_t addr = ST25R39_MODE_READ(reg);

	return spi_write_then_read(drvdata->spi, &addr, sizeof(addr), val, sizeof(*val));
}

static int st25r39_write(struct st25r39_data *drvdata, uint8_t reg, uint8_t val)
{
	uint8_t buf[] = { ST25R39_MODE_WRITE(reg), val };

	return spi_write(drvdata->spi, buf, ARRAY_SIZE(buf));
}

static int st25r39_set_bits(struct st25r39_data *drvdata, uint8_t reg, uint8_t val)
{
	uint8_t buf;
	int ret;

	ret = st25r39_read(drvdata, reg, &buf);
	if (ret)
		return ret;

	return st25r39_write(drvdata, reg, buf | val);
}

static int st25r39_clear_bits(struct st25r39_data *drvdata, uint8_t reg, uint8_t val)
{
	uint8_t buf;
	int ret;

	ret = st25r39_read(drvdata, reg, &buf);
	if (ret)
		return ret;

	return st25r39_write(drvdata, reg, buf & ~val);
}

static int st25r39_modify_bits(struct st25r39_data *drvdata, uint8_t reg,
                               uint8_t val, uint8_t mask)
{
	uint8_t buf;
	int ret;

	ret = st25r39_read(drvdata, reg, &buf);
	if (ret)
		return ret;

	return st25r39_write(drvdata, reg, (buf & ~mask) | val);
}

static int st25r39_write_chained_cmd(struct st25r39_data *drvdata,
				     uint8_t cmd, uint8_t reg, uint8_t val)
{
	uint8_t buf[] = { cmd, ST25R39_MODE_WRITE(reg), val };

	return st25r39_spi_xfer(drvdata, &buf, sizeof(buf));
}

static int st25r39_init_oscillator(struct st25r39_data *drvdata)
{
	uint8_t irq;
	int ret = 0;

	dev_dbg(drvdata->dev, "Setting clock frequency to: %u\n", drvdata->clk_freq);
	if (drvdata->clk_freq > ST25R39_CLOCK_FREQUENCY_13MHz)
		ret = st25r39_set_bits(drvdata, ST25R39_IO_CONF1, ST25R39_IO_CONF1_OSC);
	else
		ret = st25r39_clear_bits(drvdata, ST25R39_IO_CONF1, ST25R39_IO_CONF1_OSC);
	if (ret) {
		dev_err(drvdata->dev, "Failed to specify input clock frequency\n");
		return ret;
	}

	/* Enable irq */
	ret = st25r39_clear_bits(drvdata, ST25R39_MASK_MAIN_IRQ, ST25R39_MAIN_IRQ_OSC);
	if (ret) {
		dev_err(drvdata->dev, "Failed to unmask oscillator stable IRQ\n");
		return ret;
	}

	/* Enable crystal */
	ret = st25r39_set_bits(drvdata, ST25R39_OPERATION_CTL, ST25R39_OPERATION_CTL_EN);
	if (ret) {
		dev_err(drvdata->dev, "Failed to enable 'ready mode'\n");
		return ret;
	}

	/* Datasheet says 700µs, so start safe at 1ms up to 2ms */
	usleep_range(1000, 2000);

	ret = st25r39_read(drvdata, ST25R39_MAIN_IRQ, &irq);
	if (ret) {
		dev_err(drvdata->dev, "Unable to read main IRQ register\n");
		return ret;
	}

	if (!(irq & ST25R39_MAIN_IRQ_OSC)) {
		dev_err(drvdata->dev, "Oscillator failed to stabilize in time\n");
		return -EIO;
	}

	return ret;
}

static int st25r39_adjust_regulators(struct st25r39_data *drvdata, const uint16_t vdd)
{
	uint8_t irq = 0;
	uint8_t reg;
	int ret;

	ret = st25r39_read(drvdata, ST25R39_REG_VOLT_CTL, &reg);
	if (ret) {
		dev_err(drvdata->dev, "Failed get voltage regulation status\n");
		return ret;
	}

	if (!(reg & ST25R39_REG_VOLT_CTL_REG_S)) {
		uint16_t v_reg;

		ret = st25r39_clear_bits(drvdata, ST25R39_MASK_TIMER_NFC_IRQ,
					 ST25R39_MASK_TIMER_NFC_IRQ_DCT);
		if (ret) {
			dev_err(drvdata->dev,
			        "Failed to unmask direct command complete irq\n");
			return ret;
		}

		ret = st25r39_cmd(drvdata,
				  ST25R39_DIRECT_CMD_ADJUST_REGULATORS);
		if (ret) {
			dev_err(drvdata->dev,
			        "Failed to request regulator adjustment\n");
			return ret;
		}

		/* Datasheet says max 5ms */
		usleep_range(5000, 10000);

		ret = st25r39_read(drvdata, ST25R39_TIMER_NFC_IRQ, &irq);
		if (ret) {
			dev_err(drvdata->dev, "Failed to read DCT IRQ\n");
			return ret;
		}

		if (!(irq & ST25R39_TIMER_NFC_IRQ_DCT)) {
			dev_err(drvdata->dev,
			        "Regulator adjustment DCT not triggered\n");
			return -EIO;
		}

		ret = st25r39_read(drvdata, ST25R39_REG_TIMER_DISPLAY, &reg);
		if (ret) {
			dev_err(drvdata->dev, "Failed to measure regulators\n");
			return ret;
		}

		reg = ST25R39_REG_TIMER_DISPLAY_REG_GET(reg);
		if (vdd < ST25R39_IO_CONF2_SUP3V_INPUT_VOLTAGE_MAX)
			v_reg = ST25R39_REG_TIMER_DISPLAY_REG_VAL_3V3(reg);
		else
			v_reg = ST25R39_REG_TIMER_DISPLAY_REG_VAL_5V0(reg);

		dev_dbg(drvdata->dev, "Measured adjusted regulators at %d mV\n", v_reg);
	}

	return ret;
}

static int st25r39_calibrate_antenna(struct st25r39_data *drvdata)
{
	int ret;
	uint8_t reg = 0;

	ret = st25r39_clear_bits(drvdata, ST25R39_MASK_TIMER_NFC_IRQ,
				 ST25R39_MASK_TIMER_NFC_IRQ_DCT);
	if (ret) {
		dev_err(drvdata->dev, "Failed to unmask antenna DCT\n");
		goto error;
	}

	ret = st25r39_read(drvdata, ST25R39_TIMER_NFC_IRQ, &reg);
	if (ret) {
		dev_err(drvdata->dev, "Failed to read DCT IRQ\n");
		goto error;
	}

	ret = st25r39_read(drvdata, ST25R39_ANT_CAL_CTL, &reg);
	if (ret) {
		dev_err(drvdata->dev,
		        "Failed to read antenna calibration control register\n");
		goto error;
	}

	if (reg & ST25R39_ANT_CAL_CTL_TRIM_S) {
		dev_err(drvdata->dev,
			"Antenna calibration not available, LC trim switches defined manually\n");
		ret = -EACCES;
		goto error;
	}

	ret = st25r39_cmd(drvdata, ST25R39_DIRECT_CMD_CAL_ANT);
	if (ret) {
		dev_err(drvdata->dev, "Failed to request antenna calibration\n");
		goto error;
	}

	/* Reference example uses 10ms */
	usleep_range(10000, 15000);
	ret = st25r39_read(drvdata, ST25R39_TIMER_NFC_IRQ, &reg);
	if (ret) {
		dev_err(drvdata->dev, "Failed to read DCT IRQ\n");
		goto error;
	}

	if (!(reg & ST25R39_TIMER_NFC_IRQ_DCT)) {
		dev_err(drvdata->dev, "Antenna calibration DCT not triggered\n");
		ret = -EIO;
		goto error;
	}

	ret = st25r39_read(drvdata, ST25R39_ANT_CAL_DISPLAY, &reg);
	if (ret) {
		dev_err(drvdata->dev, "Failed to read Calibration status\n");
		goto error;
	}

	dev_dbg(drvdata->dev, "Antenna calibration result: %#x\n", reg);
	if (reg & ST25R39_ANT_CAL_DISPLAY_TRI_ERR) {
		dev_err(drvdata->dev, "Antenna calibration failed!\n");
		return -EIO;
	}

	return 0;

error:
	st25r39_set_bits(drvdata, ST25R39_MASK_TIMER_NFC_IRQ, ST25R39_MASK_TIMER_NFC_IRQ_DCT);
	dev_err(drvdata->dev, "Antenna calibration failed\n");
	return ret;
}

static int st25r39_rf_on(struct st25r39_data *drvdata)
{
	return st25r39_set_bits(drvdata, ST25R39_OPERATION_CTL,
				(ST25R39_OPERATION_CTL_RX_EN | ST25R39_OPERATION_CTL_TX_EN));
}

static int st25r39_rf_off(struct st25r39_data *drvdata)
{
	return st25r39_clear_bits(drvdata, ST25R39_OPERATION_CTL,
				  (ST25R39_OPERATION_CTL_RX_EN | ST25R39_OPERATION_CTL_TX_EN));
}

static int st25r39_nfc_initial_field_on(struct st25r39_data *drvdata)
{
	uint8_t val;
	int ret;

	/*
	 * Enable interrupts for detection of collision during
	 * RF Collision Avoidance and after minimum guard time expiration
	 */
	ret = st25r39_clear_bits(drvdata, ST25R39_MASK_TIMER_NFC_IRQ,
				 (ST25R39_MASK_TIMER_NFC_IRQ_CAC | ST25R39_MASK_TIMER_NFC_IRQ_CAT));
	if (ret) {
		dev_err(drvdata->dev, "Failed to unmask CAC and CAT irq\n");
		return ret;
	}

	/* Clear pending interrupts */
	ret = st25r39_read(drvdata, ST25R39_TIMER_NFC_IRQ, &val);
	if (ret) {
		dev_err(drvdata->dev, "Failed to clear timer nfc interrupts register\n");
		return ret;
	}

	ret = st25r39_modify_bits(drvdata, ST25R39_EXT_FIELD_DET_THRES,
	ST25R39_EXT_FIELD_DET_THRES_TRG_L_SET(ST25R39_EXT_FIELD_DET_THRES_75mV),
	ST25R39_EXT_FIELD_DET_THRES_TRG_L_MASK);
	if (ret) {
		dev_err(drvdata->dev,
			"Failed to set peer detection threshold\n");
		return ret;
	}

	ret = st25r39_modify_bits(drvdata, ST25R39_EXT_FIELD_DET_THRES,
	ST25R39_EXT_FIELD_DET_THRES_RFE_T_SET(ST25R39_EXT_FIELD_DET_THRES_75mV),
	ST25R39_EXT_FIELD_DET_THRES_RFE_T_MASK);
	if (ret) {
		dev_err(drvdata->dev,
			"Failed to set collision detection threshold\n");
		return ret;
	}

	/* Set n x TRFW */
	ret = st25r39_modify_bits(drvdata, ST25R39_AUX_DEF,
				  ST25R39_AUX_DEF_NFC_N_SET(0),
				  ST25R39_AUX_DEF_NFC_N_MASK);
	if (ret) {
		dev_err(drvdata->dev, "Failed to set n*TRFW\n");
		return ret;
	}

	ret = st25r39_cmd(drvdata, ST25R39_DIRECT_CMD_NFC_INIT_FIELD_ON);
	if (ret) {
		nfc_err(drvdata->dev, "Failed to turn nfc response field on\n");
		return ret;
	}

	/* Timeout for collision avoidance direct command */
	usleep_range(10000, 11000);

	ret = st25r39_read(drvdata, ST25R39_TIMER_NFC_IRQ, &val);
	if (ret) {
		dev_err(drvdata->dev, "Failed to read timer nfc irq register\n");
		return ret;
	}

	/* Disable collision detection specific interrupts */
	ret = st25r39_set_bits(drvdata, ST25R39_MASK_TIMER_NFC_IRQ,
			       (ST25R39_MASK_TIMER_NFC_IRQ_CAC |
				ST25R39_MASK_TIMER_NFC_IRQ_CAT));
	if (ret) {
		dev_err(drvdata->dev, "Failed to mask CAC and CAT irq\n");
		return ret;
	}

	if ((val & ST25R39_TIMER_NFC_IRQ_CAT))
		return st25r39_rf_on(drvdata);

	if (val & ST25R39_TIMER_NFC_IRQ_CAC) {
		dev_err(drvdata->dev,
		"An external field was detected during RF Collision Avoidance\n");
		ret = -EAGAIN;
	}

	return ret;
}

static int st25r39_set_no_response_timer(struct st25r39_data *drvdata,
					 uint32_t response_time, bool emv_mode)
{
	int ret;

	if (emv_mode) {
		ret = st25r39_set_bits(drvdata, ST25R39_GP_NO_RESP_TIMER,
				       ST25R39_GP_NO_RESP_TIMER_NRT_EMV);
		if (ret) {
			dev_err(drvdata->dev, "Failed to disable EMV mode\n");
			return ret;
		}
	} else {
		ret = st25r39_clear_bits(drvdata, ST25R39_GP_NO_RESP_TIMER,
					 ST25R39_GP_NO_RESP_TIMER_NRT_EMV);
		if (ret) {
			dev_err(drvdata->dev, "Failed to disable EMV mode\n");
			return ret;
		}
	}

	if (response_time > USHRT_MAX) {
		ret = st25r39_set_bits(drvdata, ST25R39_GP_NO_RESP_TIMER,
				       ST25R39_GP_NO_RESP_TIMER_NRT_STEP);
		if (ret) {
			dev_err(drvdata->dev, "Failed to set step in No-response timer mode\n");
			return ret;
		}

		response_time = (response_time + (ST25R39_1FC_IN_64FC - 1)) / ST25R39_1FC_IN_64FC;
		if (response_time > USHRT_MAX)
			response_time = USHRT_MAX;
	} else {
		ret = st25r39_clear_bits(drvdata, ST25R39_GP_NO_RESP_TIMER, ST25R39_GP_NO_RESP_TIMER_NRT_STEP);
		if (ret) {
			dev_err(drvdata->dev, "Failed to clear bits in No-response timer mode\n");
			return ret;
		}
	}

	ret = st25r39_write(drvdata, ST25R39_NO_RESP_TIMER1,
			    ST25R39_NO_RESP_TIMER_SET_HI(response_time));
	if (ret) {
		dev_err(drvdata->dev, "Unable to set no response timer 1\n");
		return ret;
	}

	ret = st25r39_write(drvdata, ST25R39_NO_RESP_TIMER2,
			    ST25R39_NO_RESP_TIMER_SET_LO(response_time));
	if (ret) {
		dev_err(drvdata->dev, "Unable to set no response timer 2\n");
		return ret;
	}

	return ret;
}

static int st25r39_set_nfc_short_framing(struct st25r39_data *drvdata)
{
	int ret;
	uint32_t no_rsp_timer;
	u32 mask_rx_timer;

	ret = st25r39_set_bits(drvdata, ST25R39_ISO1443A_NFC106, ST25R39_ISO1443A_NFC106_ANTCL);
	if (ret) {
		dev_err(drvdata->dev, "Failed to enable anticollision framing\n");
		return ret;
	}

	ret = st25r39_clear_bits(drvdata, ST25R39_AUX_DEF, ST25R39_AUX_DEF_CRC_2_FIFO);
	if (ret) {
		dev_err(drvdata->dev, "Failed to enable fifo crc bytes\n");
		return ret;
	}

	ret = st25r39_set_bits(drvdata, ST25R39_AUX_DEF, ST25R39_AUX_DEF_NO_CRC_RX);
	if (ret) {
		dev_err(drvdata->dev, "Failed to disable rx with crc\n");
		return ret;
	}

	/*
	 * Set mask receive timer. This timer is used to mask rx
	 * after specific period of time after tx completion
	 */
	mask_rx_timer = ST25R39_NFCA_MIN_LISTEN_FDT -
			(ST25R39_FDT_ADJUST + ST25R39_NFCA_POLL_FTD_ADJUSTMENT);

	ret = st25r39_write(drvdata, ST25R39_MASK_RECV_TIMER,
			    ST25R39_FC_TO_64FC(mask_rx_timer));
	if (ret) {
		dev_err(drvdata->dev, "Failed to set the mask receive timer\n");
		return ret;
	}

	/* Set NRT */
	no_rsp_timer = ST25R39_FC_TO_64FC(ST25R39_NFCA_LISTEN_FDT +
					  ST25R39_FDT_ADJUST +
					  ST25R39_NFCA_FWT_A_ADJUSTMENT);
	/* no_rsp_timer = ST25R39_FC_TO_64FC(1620 + 64 + 512); */
	ret = st25r39_set_no_response_timer(drvdata, no_rsp_timer, false);
	if (ret) {
		dev_err(drvdata->dev, "Failed to set the no response timer\n");
		return ret;
	}

	ret = st25r39_set_bits(drvdata, ST25R39_RECV_CONF2, ST25R39_RECV_CONF2_AGC_EN);
	if (ret) {
		dev_err(drvdata->dev, "Failed to enable agc\n");
		return ret;
	}

	return ret;
}

static int st25r39_read_fifo(struct st25r39_data *drvdata)
{
	struct sk_buff *skb = drvdata->skb_resp;
	uint8_t fifo_stat_2;
	uint8_t fifo_b;
	int ret;

	if (!drvdata->skb_resp) {
		dev_dbg(drvdata->dev, "skb_resp is null in read_fifo\n");
		return -EIO;
	}

	ret = st25r39_read(drvdata, ST25R39_FIFO_STATUS1, &fifo_b);
	if (ret) {
		dev_err(drvdata->dev, "Unable to get FIFO status 1\n");
		return ret;
	}

	fifo_b = fifo_b & ST25R39_FIFO_STATUS1_FIFO_B_MASK;

	if (!fifo_b) {
		dev_err(drvdata->dev, "No bytes to read in FIFO\n");
		return -EAGAIN;
	}

	/* Both FIFO status register needs to be read before reading fifo */
	ret = st25r39_read(drvdata, ST25R39_FIFO_STATUS2, &fifo_stat_2);
	if (ret) {
		dev_err(drvdata->dev, "Unable to get FIFO status 2\n");
		return ret;
	}

	return st25r39_spi_fifo_read(drvdata, skb_put(skb, fifo_b), fifo_b);
}

static int st25r39_load_fifo(struct st25r39_data *drvdata,
			     struct sk_buff *skb)
{
	uint8_t fifo_b;
	int ret;

	ret = st25r39_read(drvdata, ST25R39_FIFO_STATUS1, &fifo_b);
	if (ret) {
		dev_err(drvdata->dev, "Unable to get FIFO status");
		return ret;
	}

	if (fifo_b & ST25R39_FIFO_STATUS1_FIFO_B_MASK) {
		dev_err(drvdata->dev, "FIFO not empty, %#x bytes remaining", fifo_b);
		return -EINVAL;
	}

	if (skb->len > ST25R39_FIFO_DEPTH_MAX) {
		dev_err(drvdata->dev,
			"Large fifo transfers not yet supported, %#x",
			skb->len);
		return -EIO;
	}

	/* Workaround for silicon bug, reset nbtx bits or we get parity err */
	ret = st25r39_clear_bits(drvdata, ST25R39_NUM_TX_BYTES2,
				 ST25R39_NUM_TX_BYTES2_NBTX_MASK);
	if (ret) {
		dev_err(drvdata->dev, "Failed to clear nbtx bits");
		return ret;
	}

	ret = st25r39_write(drvdata, ST25R39_NUM_TX_BYTES1,
			    ST25R39_NUM_TX_BYTES_NTX_SET_HI(skb->len));
	if (ret) {
		dev_err(drvdata->dev, "Failed to set hi tx bytes");
		return ret;
	}

	ret = st25r39_write(drvdata, ST25R39_NUM_TX_BYTES2,
			    ST25R39_NUM_TX_BYTES_NTX_SET_LO(skb->len));
	if (ret) {
		dev_err(drvdata->dev, "Failed to set lo tx bytes");
		return ret;
	}

	/* Add appropriate fifo load command in front of spi buffer */
	*(uint8_t *)skb_push(skb, ST25R39_HEADROOM_LEN) = ST25R39_MODE_FIFO_LOAD;

	print_hex_dump_debug("fifo load data: ", DUMP_PREFIX_NONE, 16, 1,
			     skb->data, skb->len, false);

	return spi_write(drvdata->spi, skb->data, skb->len);
}

static int st25r39_clear_all_interrupts(struct st25r39_data *drvdata)
{
	uint8_t reg;
	int ret;

	ret = st25r39_read(drvdata, ST25R39_MAIN_IRQ, &reg);
	if (ret) {
		dev_err(drvdata->dev, "Unable to read main irq register\n");
		return ret;
	}

	ret = st25r39_read(drvdata, ST25R39_TIMER_NFC_IRQ, &reg);
	if (ret) {
		dev_err(drvdata->dev, "Unable to read timer nfc irq register\n");
		return ret;
	}

	ret = st25r39_read(drvdata, ST25R39_ERROR_WAKEUP_IRQ, &reg);
	if (ret) {
		dev_err(drvdata->dev, "Unable to read error wakeup irq register\n");
		return ret;
	}

	return ret;
}

static int st25r39_disable_all_interrupts(struct st25r39_data *drvdata)
{
	int ret;

	ret = st25r39_write(drvdata, ST25R39_MASK_MAIN_IRQ, ST25R39_MASK_MAIN_IRQ_ALL);
	if (ret) {
		dev_err(drvdata->dev, "Failed to mask main interrupts\n");
		return ret;
	}

	ret = st25r39_write(drvdata, ST25R39_MASK_TIMER_NFC_IRQ, ST25R39_MASK_TIMER_NFC_IRQ_ALL);
	if (ret) {
		dev_err(drvdata->dev, "Failed to mask timer nfc interrupts\n");
		return ret;
	}

	ret = st25r39_write(drvdata, ST25R39_MASK_ERROR_WAKEUP_IRQ,
			    ST25R39_MASK_ERROR_WAKEUP_IRQ_ALL);
	if (ret) {
		dev_err(drvdata->dev, "Failed to mask error wakeup interrupts\n");
		return ret;
	}

	return ret;
}

static int st25r39_send_cmd(struct st25r39_data *drvdata, struct sk_buff *skb,
			    u16 timeout, nfc_digital_cmd_complete_t cb,
			    void *arg)
{
	int ret;
	uint8_t val;

	ret = st25r39_read(drvdata, ST25R39_OPERATION_CTL, &val);
	if (ret) {
		dev_err(drvdata->dev, "Failed to read OPERATION_CTL\n");
		ret = -EIO;
		goto exit;
	}
	if (!(val & ST25R39_OPERATION_CTL_TX_EN)) {
		cancel_delayed_work(&drvdata->timeout_work);
		dev_err(drvdata->dev,
			"tx is not on, but it should have turned on by RF initial field\n");
		ret = -EIO;
		goto exit;
	}

	/* Turn rx on, tx is already on with rf initial field */
	ret = st25r39_set_bits(drvdata, ST25R39_OPERATION_CTL, ST25R39_OPERATION_CTL_RX_EN);
	if (ret) {
		dev_err(drvdata->dev, "Failed to enable rx\n");
		goto exit;
	}

	ret = st25r39_cmd(drvdata, ST25R39_DIRECT_CMD_CLEAR);
	if (ret) {
		dev_err(drvdata->dev, "Failed to clear fifo\n");
		goto exit;
	}

	ret = st25r39_cmd(drvdata, ST25R39_DIRECT_CMD_SQUELCH);
	if (ret) {
		dev_err(drvdata->dev, "Failed to squelch rx gain\n");
		goto exit;
	}

	usleep_range(500, 1000);

	ret = st25r39_disable_all_interrupts(drvdata);
	if (ret) {
		dev_err(drvdata->dev, "Failed to disable all interrupts\n");
		goto exit;
	}

	ret = st25r39_clear_all_interrupts(drvdata);
	if (ret) {
		dev_err(drvdata->dev, "Failed to clear all interrupts\n");
		goto exit;
	}

	/* Unmask needed interrupts */
	ret = st25r39_clear_bits(drvdata, ST25R39_MASK_MAIN_IRQ,
				 (ST25R39_MASK_MAIN_IRQ_RXE |
	                         ST25R39_MASK_MAIN_IRQ_RXS |
				 ST25R39_MASK_MAIN_IRQ_COL));
	if (ret) {
		dev_err(drvdata->dev, "Failed to unmask main irq\n");
		goto exit;
	}

	ret = st25r39_clear_bits(drvdata, ST25R39_MASK_TIMER_NFC_IRQ,
				 ST25R39_MASK_TIMER_NFC_IRQ_NRE | ST25R39_MASK_TIMER_NFC_IRQ_EOF);
	if (ret) {
		dev_err(drvdata->dev, "Failed to unmask nre irq\n");
		goto exit;
	}

	ret = st25r39_clear_bits(drvdata, ST25R39_MASK_ERROR_WAKEUP_IRQ,
	                         ST25R39_MASK_ERROR_WAKEUP_IRQ_CRC |
	                         ST25R39_MASK_ERROR_WAKEUP_IRQ_PAR |
	                         ST25R39_MASK_ERROR_WAKEUP_IRQ_ERR2 |
	                         ST25R39_MASK_ERROR_WAKEUP_IRQ_ERR1);
	if (ret) {
		dev_err(drvdata->dev, "Failed to unmask error wakeup irq\n");
		goto exit;
	}

	/*
	 * The number of valid bits in the last byte must be set to
	 * zero nbtx<2:0> Page 29
	 */
	ret = st25r39_write(drvdata, ST25R39_NUM_TX_BYTES2, 0);
	if (ret) {
		dev_err(drvdata->dev, "Failed to clear num tx 2 register bits\n");
		goto exit;
	}

	ret = down_killable(&drvdata->exchange_lock);
	if (ret) {
		dev_err(drvdata->dev, "%s: Failed to lower semaphore\n", __func__);
		return ret;
	}

	drvdata->complete_cb = cb;
	drvdata->cb_arg = arg;
	if (skb->data[0] == ST25R39_REQA) {
		ret = st25r39_cmd(drvdata, ST25R39_DIRECT_CMD_XMIT_REQA);
		if (ret) {
			dev_err(drvdata->dev, "Failed to send REQA\n");
			goto exit;
		}
	} else {
		ret = st25r39_load_fifo(drvdata, skb);
		if (ret) {
			dev_err(drvdata->dev, "Failed to load fifo\n");
			goto exit;
		}

		if ((drvdata->framing == NFC_DIGITAL_FRAMING_NFCA_STANDARD_WITH_CRC_A) ||
		    (drvdata->framing == NFC_DIGITAL_FRAMING_NFCA_T4T)) {
			ret = st25r39_cmd(drvdata,
					  ST25R39_DIRECT_CMD_XMIT_WITH_CRC);
			if (ret) {
				dev_err(drvdata->dev,
					"Failed execute 'Transmit with CRC' cmd\n");
				goto exit;
			}
		} else {
			ret = st25r39_cmd(drvdata,
					  ST25R39_DIRECT_CMD_XMIT_WITHOUT_CRC);
			if (ret) {
				dev_err(drvdata->dev,
					"Failed execute 'Transmit w/o CRC' cmd\n");
				goto exit;
			}
		}
	}

exit:
	if ((ret != -EIO) && (skb))
		dev_kfree_skb_any(skb);
	return ret;
}

static int _digital_layer_send_data(struct st25r39_data *drvdata) {
	int ret;
	drvdata->skb_resp = nfc_alloc_recv_skb(ST25R39_FIFO_DEPTH_MAX, GFP_KERNEL);
	if (!drvdata->skb_resp) {
		nfc_err(drvdata->dev, "Can't alloc skb_resp in %s\n", __func__);
		return -ENOMEM;
	}

	ret = st25r39_read_fifo(drvdata);
	if (ret) {
		dev_err(drvdata->dev, "%s: failed to read fifo\n", __func__);
		if (!IS_ERR_OR_NULL(drvdata->skb_resp)) {
			kfree_skb(drvdata->skb_resp);
			drvdata->skb_resp = NULL;
		}
		return -EIO;
	}

	print_hex_dump_debug("read fifo data: ", DUMP_PREFIX_NONE, 16,
			    1, drvdata->skb_resp->data,
			    drvdata->skb_resp->len, false);

	/* call digital layer callback */
	drvdata->complete_cb(drvdata->nfc_ddev, drvdata->cb_arg,
			drvdata->skb_resp);
	drvdata->skb_resp = NULL;

	up(&drvdata->exchange_lock);
	return ret;
}

static void st25r39_timeout_work_handler(struct work_struct *work)
{
	struct st25r39_data *drvdata = container_of(work, struct st25r39_data,
					    timeout_work.work);

	dev_info(drvdata->dev, "%s\n", __func__);

	mutex_lock(&drvdata->lock);
	if (drvdata->rxs_signaled) {
		dev_info(drvdata->dev, "%s: errata 1.1: I_rxe was not signaled \n", __func__);
		_digital_layer_send_error(drvdata);
		drvdata->rxs_signaled = false;
	}
	mutex_unlock(&drvdata->lock);
}

static int st25r39_init(struct st25r39_data *drvdata)
{
	uint8_t chipid = 0;
	uint16_t vdd;
	uint8_t irq;
	uint8_t reg;
	int ret;

	/* Read IC identity */
	ret = st25r39_read(drvdata, ST25R39_IC_IDENTITY, &chipid);
	if (ret && !chipid) {
		dev_err(drvdata->dev, "Unable to read IC identity\n");
		return ret;
	}

	/* Set all to default */
	ret = st25r39_cmd(drvdata, ST25R39_DIRECT_CMD_SET_DEFAULT);
	if (ret) {
		dev_err(drvdata->dev, "Failed to reset chip\n");
		return ret;
	}

	/*
	 * Set IO configuration 1 & 2 and Operation Control registers
	 * to it's default values.
	 * Those registers are not set to default after warm reset
	 */
	ret = st25r39_write(drvdata, ST25R39_IO_CONF1,
			    ST25R39_IO_CONF1_DEFAULT_SETTINGS);
	if (ret) {
		dev_err(drvdata->dev, "Failed to set io Configuration #1 register to defaults\n");
		return ret;
	}

	ret = st25r39_clear_bits(drvdata, ST25R39_IO_CONF2,
	                         ST25R39_IO_CONF2_SUP3V |
	                         ST25R39_IO_CONF2_VSPD_OFF |
	                         ST25R39_IO_CONF2_MISO_PD2 |
	                         ST25R39_IO_CONF2_MISO_PD1 |
	                         ST25R39_IO_CONF2_IO18 |
	                         ST25R39_IO_CONF2_SLOW_UP);
	if (ret) {
		dev_err(drvdata->dev, "Failed to set io Configuration #2 register to defaults\n");
		return ret;
	}

	ret = st25r39_clear_bits(drvdata, ST25R39_OPERATION_CTL,
	                         ST25R39_OPERATION_CTL_EN |
	                         ST25R39_OPERATION_CTL_RX_EN |
	                         ST25R39_OPERATION_CTL_RX_CHN |
	                         ST25R39_OPERATION_CTL_RX_MAN |
	                         ST25R39_OPERATION_CTL_TX_EN |
	                         ST25R39_OPERATION_CTL_WU);
	if (ret) {
		dev_err(drvdata->dev, "Failed to set operation control register to defaults\n");
		return ret;
	}

	ret = st25r39_disable_all_interrupts(drvdata);
	if (ret) {
		dev_err(drvdata->dev, "Failed to disable all interrupts\n");
		return ret;
	}

	ret = st25r39_clear_all_interrupts(drvdata);
	if (ret) {
		dev_err(drvdata->dev, "Failed to clear all interrupts\n");
		return ret;
	}

	ret = st25r39_set_bits(drvdata, ST25R39_IO_CONF2,
			       ST25R39_IO_CONF2_MISO_PD2 |
			       ST25R39_IO_CONF2_MISO_PD1);
	if (ret) {
		dev_err(drvdata->dev, "%s: Failed to set bits\n", __func__);
		return ret;
	}

	/* TODO trim settings for VHBR board, will anyway changed later on */
	ret = st25r39_write(drvdata, ST25R39_ANT_CAL_TARGET_CTL, 0x80);
	if (ret) {
		dev_err(drvdata->dev, "%s: Unable to write \n", __func__);
		return ret;
	}

	/* Initialize oscillator */
	ret = st25r39_init_oscillator(drvdata);
	if (ret) {
		dev_err(drvdata->dev, "Failed to setup oscillator\n");
		return ret;
	}

	/* Measure power supply VDD */
	ret = st25r39_set_bits(drvdata, ST25R39_REG_VOLT_CTL,
	                       ST25R39_REG_VOLT_CTL_MPSV_SET(ST25R39_REG_VOLT_CTL_MPSV_VDD));
	if (ret) {
		dev_err(drvdata->dev, "Failed to set mpsv\n");
		return ret;
	}

	ret = st25r39_clear_bits(drvdata, ST25R39_MASK_TIMER_NFC_IRQ,
				 ST25R39_MASK_TIMER_NFC_IRQ_DCT);
	if (ret) {
		dev_err(drvdata->dev, "Failed to unmask direct command complete IRQ\n");
		return ret;
	}

	ret = st25r39_cmd(drvdata, ST25R39_DIRECT_CMD_MEASURE_PSU);
	if (ret) {
		dev_err(drvdata->dev, "Failed to request power measurement\n");
		return ret;
	}

	/* Datasheet says max 25µs, so start safe at 250µs up to 500µs */
	usleep_range(250, 500);

	irq = 0;
	ret = st25r39_read(drvdata, ST25R39_TIMER_NFC_IRQ, &irq);
	if (ret) {
		dev_err(drvdata->dev, "Failed to read DCT IRQ\n");
		return ret;
	}

	if (!(irq & ST25R39_TIMER_NFC_IRQ_DCT)) {
		dev_err(drvdata->dev, "Oscillator DCT not triggered\n");
		return -EIO;
	}

	ret = st25r39_read(drvdata, ST25R39_AD_CONV_OUT, &reg);
	if (ret) {
		dev_err(drvdata->dev, "Failed to measure vdd\n");
		return ret;
	}

	vdd = ST25R39_AD_CONV_OUT_mV(reg);
	dev_dbg(drvdata->dev, "Measured vdd at %d mV\n", vdd);

	if (vdd < ST25R39_IO_CONF2_SUP3V_INPUT_VOLTAGE_MAX)
		ret = st25r39_set_bits(drvdata, ST25R39_IO_CONF2, ST25R39_IO_CONF2_SUP3V);
	else
		ret = st25r39_clear_bits(drvdata, ST25R39_IO_CONF2, ST25R39_IO_CONF2_SUP3V);

	if (ret) {
		dev_err(drvdata->dev, "Failed to select supply voltage\n");
		return ret;
	}

	/* Make sure tx and rx are disabled */
	ret = st25r39_clear_bits(drvdata, ST25R39_OPERATION_CTL,
	                         ST25R39_OPERATION_CTL_RX_EN |
	                         ST25R39_OPERATION_CTL_TX_EN);
	if (ret) {
		dev_err(drvdata->dev, "Failed to turn of rx and tx\n");
		return ret;
	}

	/* Set water levels for FIFO */
	ret = st25r39_modify_bits(drvdata, ST25R39_IO_CONF1,
	                          ST25R39_IO_CONF1_FIFO_LR_SET(64) | ST25R39_IO_CONF1_FIFO_LT_SET(32),
	                          ST25R39_IO_CONF1_FIFO_LR | ST25R39_IO_CONF1_FIFO_LT);
	if (ret) {
		dev_err(drvdata->dev, "Failed to set fifo levels\n");
		return ret;
	}

	ret = st25r39_write_chained_cmd(drvdata, ST25R39_DIRECT_CMD_TEST_ACCESS,
	                                ST25R39_ANALOG_TEST_OBS,
	                                ST25R39_ANALOG_TEST_OBS_TANA_SET(ST25R39_ANALOG_TEST_OBS_TANA_DISABLE));
	if (ret) {
		dev_err(drvdata->dev, "Failed to disable observation mode\n");
		return ret;
	}

	/* Disable MCU_CLK output */
	ret = st25r39_modify_bits(drvdata, ST25R39_IO_CONF1,
	                          ST25R39_IO_CONF1_OUT_CL_SET(ST25R39_IO_CONF1_OUT_CL_DISABLE) |
	                          ST25R39_IO_CONF1_LF_CLK_OFF,
	                          ST25R39_IO_CONF1_OUT_CL_MASK | ST25R39_IO_CONF1_LF_CLK_OFF);
	if (ret) {
		dev_err(drvdata->dev, "Failed to disable MCU_CLK output\n");
		return ret;
	}

	ret = st25r39_adjust_regulators(drvdata, vdd);
	if (ret)
		dev_info(drvdata->dev, "%s: Regulator adjast failed \n", __func__);

	if (drvdata->config->has_aat) {
		/* Work-around for silicon bug errata #1.5, Run calibration twice. */
		ret = st25r39_calibrate_antenna(drvdata);
		if (ret) {
			dev_err(drvdata->dev, "Failed to calibrate antenna\n");
			return ret;
		}

		ret = st25r39_calibrate_antenna(drvdata);
		if (ret) {
			dev_err(drvdata->dev, "Failed to calibrate antenna\n");
			return ret;
		}

		/* Adjust regulator after antenna calibration */
		ret = st25r39_adjust_regulators(drvdata, vdd);
		if (ret)
			dev_info(drvdata->dev, "%s: Regulator adjast failed \n", __func__);
	}

	dev_info(drvdata->dev,
		 "Initialized ST25R39xx: 0x%x %#02lx, rev: %#02lx", chipid,
		 ST25R39_IC_IDENTITY_TYPE(chipid),
		 ST25R39_IC_IDENTITY_REV(chipid));

	return 0;
}

static irqreturn_t st25r39_irq(int irq, void *data)
{
	struct st25r39_data *drvdata = data;
	uint8_t error_irq = 0;
	uint8_t timer_irq = 0;
	uint8_t main_irq = 0;
	int ret;

	if (!drvdata || irq != drvdata->spi->irq) {
		WARN_ON_ONCE(1);
		up(&drvdata->exchange_lock);
		return IRQ_NONE;
	}

	/*
	 * If semaphore is not down already then we don't know what
	 * context we are serving here and then we just release the
	 * semaphore and let go.
	 */
	if (!down_trylock(&drvdata->exchange_lock)) {
		cancel_delayed_work(&drvdata->timeout_work);
		drvdata->rxs_signaled = false;
		up(&drvdata->exchange_lock);
		dev_dbg(drvdata->dev, "isr: unknown drvdata\n");
		return IRQ_NONE;
	}

	mutex_lock(&drvdata->lock);
	/*
	 * Scheduled work is related just to Errata #1.1 to process missing RXE,
	 * to avoid a situation when a new interrupt appears and scheduled work
	 * is executed in the wrong moment. To overcome this scheduled work is
	 * canceled with every new interrupt.
	 */
	cancel_delayed_work(&drvdata->timeout_work);

	ret = st25r39_read(drvdata, ST25R39_MAIN_IRQ, &main_irq);
	if (ret) {
		dev_err(drvdata->dev, "isr: unable to read main IRQ register\n");
		_digital_layer_send_error(drvdata);
		goto exit;
	}

	ret = st25r39_read(drvdata, ST25R39_TIMER_NFC_IRQ, &timer_irq);
	if (ret) {
		dev_err(drvdata->dev, "isr: unable to read timer IRQ register\n");
		_digital_layer_send_error(drvdata);
		goto exit;
	}

	ret = st25r39_read(drvdata, ST25R39_ERROR_WAKEUP_IRQ, &error_irq);
	if (ret) {
		dev_err(drvdata->dev, "isr: unable to read error IRQ register\n");
		_digital_layer_send_error(drvdata);
		goto exit;
	}

	if (error_irq) {
		dev_err(drvdata->dev, "isr: error register: %#x\n", error_irq);
	}

	/*
	 * REMARK: Silicon workaround ST25R3911 Errata #1.7
	 * NRE interrupt may be triggered twice
	 * Ignore NRE if is detected together with no Rx Start
	 */
	if (timer_irq & ST25R39_TIMER_NFC_IRQ_NRE &&
 	   !(main_irq & ST25R39_MAIN_IRQ_RXS)) {
		drvdata->rxs_signaled = false;
		_digital_layer_send_error(drvdata);
		goto exit;
	}

	/* External field droped below Target activation level */
	if (timer_irq & ST25R39_TIMER_NFC_IRQ_EOF &&
	   !(main_irq & ST25R39_MAIN_IRQ_RXS)) {
		dev_info(drvdata->dev, "isr: eof\n");
		drvdata->rxs_signaled = false;
		_digital_layer_send_error(drvdata);
		goto exit;
	}

	if (main_irq & ST25R39_MAIN_IRQ_RXS) {
		if (main_irq & ST25R39_MAIN_IRQ_RXE) {
			drvdata->rxs_signaled = false;
			ret = _digital_layer_send_data(drvdata);
			if (ret) {
				dev_err(drvdata->dev, "%s: error delivering data upstream\n", __func__);
				_digital_layer_send_error(drvdata);
			}
		} else {
			/*
			 * Silicon workaround Errata #1.1
			 * Rarely on corrupted frames I_rxs gets signaled, but
			 * I_rxe is not signaled
			 * Use a SW timer to handle an eventual missing RXE
			 */
			drvdata->rxs_signaled = true;
			schedule_delayed_work(&drvdata->timeout_work,
				msecs_to_jiffies(ST25R39_NO_RXE_TIMEOUT));
		}
		goto exit;
	} else if (main_irq & ST25R39_MAIN_IRQ_RXE) {
		/*
		 * Silicon workaround Errata #1.9
		 * ST25R3911 may indicate RXE without RXS previously,
		 * this happens upon some noise or incomplete byte frames
		 * with less than 4 bits
		 */
		if (drvdata->rxs_signaled) {
			drvdata->rxs_signaled = false;
			ret = _digital_layer_send_data(drvdata);
			if (ret) {
				dev_err(drvdata->dev, "%s: error delivering data upstream, errata 1.9 \n", __func__);
				_digital_layer_send_error(drvdata);
			}
			goto exit;
		} else {
			dev_info(drvdata->dev, "%s: rxe errata 1.9\n", __func__);
			_digital_layer_send_error(drvdata);
		}
	} else {
		dev_info(drvdata->dev, "%s: undefined interrupt\n", __func__);
		_digital_layer_send_error(drvdata);
		drvdata->rxs_signaled = false;
	}

	if (main_irq & ST25R39_MAIN_IRQ_COL) {
		dev_info(drvdata->dev, "isr: bit collision\n");
		drvdata->rxs_signaled = false;
		drvdata->skb_resp = ERR_PTR(-ENODATA);
		drvdata->complete_cb(drvdata->nfc_ddev, drvdata->cb_arg,
				     drvdata->skb_resp);
		drvdata->skb_resp = NULL;
	}

exit:
	mutex_unlock(&drvdata->lock);
	return IRQ_HANDLED;
}

static int st25r39_config_rf_tech(struct st25r39_data *drvdata, int param) {
	int ret;

	ret = st25r39_cmd(drvdata, ST25R39_DIRECT_CMD_CLEAR);
	if (ret) {
		dev_err(drvdata->dev, "Failed to set analog configuration\n");
		return ret;
	}

	ret = st25r39_cmd(drvdata, ST25R39_DIRECT_CMD_UNMASK_RX_DATA);
	if (ret) {
		dev_err(drvdata->dev, "Failed to send direct command unmask rx\n");
		return ret;
	}

	/* Switch to initiator. That's the only mode supported. */
		ret = st25r39_clear_bits(drvdata, ST25R39_MODE_DEF,
		                         ST25R39_MODE_DEF_TARGET);
		if (ret) {
		dev_err(drvdata->dev, "Failed to switch to initiator mode\n");
		return ret;
		}

	switch (param) {
	case NFC_DIGITAL_RF_TECH_106A:
		st25r39_clear_bits(drvdata, ST25R39_OPERATION_CTL,
				  ST25R39_OPERATION_CTL_WU);
		ret = st25r39_modify_bits(drvdata, ST25R39_MODE_DEF,
		ST25R39_MODE_DEF_OM_SET(ST25R39_MODE_DEF_OM_IN_ISO1443A),
		ST25R39_MODE_DEF_OM_MASK);
		if (ret) {
			dev_err(drvdata->dev,
				"Failed to switch to ISO14443A mode\n");
			return ret;
		}

		ret = st25r39_modify_bits(drvdata, ST25R39_BIT_RATE_DEF,
		ST25R39_BIT_RATE_DEF_TX_RATE_SET(ST25R39_BIT_RATE_DEF_RATE_106KBPS),
		ST25R39_BIT_RATE_DEF_TX_RATE_MASK);
		if (ret) {
			dev_err(drvdata->dev, "Failed to set tx bitrate\n");
			return ret;
		}

		ret = st25r39_modify_bits(drvdata, ST25R39_BIT_RATE_DEF,
		ST25R39_BIT_RATE_DEF_RX_RATE_SET(ST25R39_BIT_RATE_DEF_RATE_106KBPS),
		ST25R39_BIT_RATE_DEF_RX_RATE_MASK);
		if (ret) {
			dev_err(drvdata->dev, "Failed to set rx bitrate\n");
			return ret;
		}

		break;
	default:
		nfc_err(drvdata->dev, "RF Technology '%d' not supported\n",
			param);
		return -EINVAL;
	}

	ret = st25r39_cmd(drvdata, ST25R39_DIRECT_CMD_ANALOG_PRESET);
	if (ret) {
		dev_err(drvdata->dev, "Failed to set analog configuration\n");
		return ret;
	}

	ret = st25r39_nfc_initial_field_on(drvdata);
	if (ret) {
		dev_err(drvdata->dev, "Failed to turn nfc initial field on\n");
		return ret;
	}

	return ret;
}


static int st25r39_config_framing(struct st25r39_data *drvdata, int param) {
	int ret = -EINVAL;
	u32 no_rsp_timer;

		switch (param) {
		case NFC_DIGITAL_FRAMING_NFCA_SHORT:
			drvdata->framing = NFC_DIGITAL_FRAMING_NFCA_SHORT;
			ret = st25r39_set_nfc_short_framing(drvdata);
			if (ret) {
				dev_err(drvdata->dev,
					"Failed to set nfc-a short frame settings\n");
				return ret;
			}

			break;
		case NFC_DIGITAL_FRAMING_NFCA_STANDARD:
			drvdata->framing = NFC_DIGITAL_FRAMING_NFCA_STANDARD;

			/*
			 * According to ST agc should be turned off after REQA
			 * and this should help during anti-collision transfer.
			 */
			ret = st25r39_clear_bits(drvdata, ST25R39_RECV_CONF2,
			                         ST25R39_RECV_CONF2_AGC_EN);
			if (ret) {
				dev_err(drvdata->dev,
					"Failed to disable agc\n");
				return ret;
			}

			ret = st25r39_set_bits(drvdata, ST25R39_AUX_DEF,
			                       ST25R39_AUX_DEF_NO_CRC_RX);
			if (ret) {
				dev_err(drvdata->dev, "Failed to disable rx with crc\n");
				return ret;
			}

			break;
		case NFC_DIGITAL_FRAMING_NFCA_T4T:
			drvdata->framing = NFC_DIGITAL_FRAMING_NFCA_T4T;
			no_rsp_timer =
			  ST25R39_FC_TO_64FC(ST25R39_ISODEP_T4T_FWT_ACTIVATION);
			dev_info(drvdata->dev, "long nre nfca t4t\n");
			ret = st25r39_set_no_response_timer(drvdata,
							   no_rsp_timer, false);
			if (ret) {
				dev_err(drvdata->dev, "Failed to set the no response timer\n");
				return ret;
			}

			ret = st25r39_set_bits(drvdata, ST25R39_AUX_DEF,
			                       ST25R39_AUX_DEF_CRC_2_FIFO);
			if (ret) {
				dev_err(drvdata->dev, "Failed to enable rx with crc\n");
				return ret;
			}

			break;

		case NFC_DIGITAL_FRAMING_NFCA_STANDARD_WITH_CRC_A: /* fall-through */
		case NFC_DIGITAL_FRAMING_NFCA_T2T: /* fall-through */
		case NFC_DIGITAL_FRAMING_NFCA_NFC_DEP:
			drvdata->framing = NFC_DIGITAL_FRAMING_NFCA_STANDARD_WITH_CRC_A;
			ret = st25r39_clear_bits(drvdata, ST25R39_ISO1443A_NFC106,
						 ST25R39_ISO1443A_NFC106_ANTCL);
			if (ret) {
				dev_err(drvdata->dev, "Failed to disable anti-collision framing\n");
				return ret;
			}

			no_rsp_timer =
			  ST25R39_FC_TO_64FC(ST25R39_ISODEP_T4T_FWT_ACTIVATION);
			dev_dbg(drvdata->dev, "long nre\n");
			ret = st25r39_set_no_response_timer(drvdata,
							   no_rsp_timer, false);
			if (ret) {
				dev_err(drvdata->dev, "Failed to set the no response timer\n");
				return ret;
			}

			ret = st25r39_clear_bits(drvdata, ST25R39_AUX_DEF,
						 ST25R39_AUX_DEF_NO_CRC_RX);
			if (ret) {
				dev_err(drvdata->dev, "Failed to enable rx with crc\n");
				return ret;
			}

			break;
		case NFC_DIGITAL_FRAMING_NFCA_ANTICOL_COMPLETE:
			dev_info(drvdata->dev, "Anticol complete\n");
			drvdata->framing = NFC_DIGITAL_FRAMING_NFCA_ANTICOL_COMPLETE;
			ret = st25r39_clear_bits(drvdata, ST25R39_ISO1443A_NFC106,
						 ST25R39_ISO1443A_NFC106_ANTCL);
			if (ret) {
				dev_err(drvdata->dev, "Failed to disable anti-collision framing\n");
				return ret;
			}

			break;
		case NFC_DIGITAL_FRAMING_NFCA_T1T:
			dev_info(drvdata->dev, "T1T type %s\n", __func__);
			break;
		default:
			dev_err(drvdata->dev, "Unsupported configuration type %d\n", param);
			break;
		}

	return ret;
}

static int st25r39_in_configure_hw(struct nfc_digital_dev *ddev, int type,
				   int param)
{
	struct st25r39_data *drvdata = nfc_digital_get_drvdata(ddev);
	int ret = -EINVAL;

	mutex_lock(&drvdata->lock);

	switch (type) {
	case NFC_DIGITAL_CONFIG_RF_TECH:
		ret = st25r39_config_rf_tech(drvdata, param);
		drvdata->rf_tech = param;
		break;
	case NFC_DIGITAL_CONFIG_FRAMING:
		ret = st25r39_config_framing(drvdata, param);
		break;
	default:
		dev_err(drvdata->dev, "Unsupported configuration type %d\n", param);
		break;
	}

	mutex_unlock(&drvdata->lock);
	return ret;
}

static int st25r39_in_send_cmd(struct nfc_digital_dev *ddev,
			       struct sk_buff *skb, u16 timeout,
			       nfc_digital_cmd_complete_t cb, void *arg)
{
	struct st25r39_data *drvdata = nfc_digital_get_drvdata(ddev);
	struct device *dev = NULL;
	int ret = -EINVAL;

	mutex_lock(&drvdata->lock);

	dev = &ddev->nfc_dev->dev;

	switch (drvdata->rf_tech) {
	case NFC_DIGITAL_RF_TECH_106A:
		ret = st25r39_send_cmd(drvdata, skb, timeout, cb, arg);
		break;
	default:
		nfc_err(dev, "Unsupported rf tech %d\n", drvdata->rf_tech);
		break;
	}

	mutex_unlock(&drvdata->lock);
	return ret;
}

static int st25r39_switch_rf(struct nfc_digital_dev *ddev, bool on)
{
	struct st25r39_data *drvdata = nfc_digital_get_drvdata(ddev);
	int ret;
	
	mutex_lock(&drvdata->lock);

	if (on)
		ret = st25r39_rf_on(drvdata);
	else
		ret = st25r39_rf_off(drvdata);

	mutex_unlock(&drvdata->lock);

	return ret;
}

static void st25r39_abort_cmd(struct nfc_digital_dev *ddev)
{
	struct st25r39_data *drvdata = nfc_digital_get_drvdata(ddev);
	struct device *dev = &ddev->nfc_dev->dev;

	nfc_info(dev, "%s\n", __func__);
	mutex_lock(&drvdata->lock);

	cancel_delayed_work(&drvdata->timeout_work);
	
	if (st25r39_cmd(drvdata, ST25R39_DIRECT_CMD_MASK_RX_DATA))
		nfc_err(dev, "Failed to send direct command mask rx\n");

	if (st25r39_cmd(drvdata, ST25R39_DIRECT_CMD_CLEAR))
		nfc_err(dev, "Failed to send direct command clear\n");

	if (!IS_ERR_OR_NULL(drvdata->skb_resp)) {
		kfree_skb(drvdata->skb_resp);
		drvdata->skb_resp = NULL;
	}

	mutex_unlock(&drvdata->lock);
}

/* Target mode - P2P is not yet supported */
static int st25r39_tg_configure_hw(struct nfc_digital_dev *ddev, int type,
				   int param)
{
	return 0;
}

static int st25r39_tg_get_rf_tech(struct nfc_digital_dev *ddev, u8 *rf_tech)
{
	return 0;
}

static int st25r39_tg_listen(struct nfc_digital_dev *ddev, u16 timeout,
			     nfc_digital_cmd_complete_t cb, void *arg)
{
	return 0;
}

static int st25r39_tg_listen_md(struct nfc_digital_dev *ddev, u16 timeout,
				nfc_digital_cmd_complete_t cb, void *arg)
{
	return 0;
}

static int st25r39_tg_send_cmd(struct nfc_digital_dev *ddev,
			       struct sk_buff *skb, u16 timeout,
			       nfc_digital_cmd_complete_t cb, void *arg)
{
	return 0;
}
/* End of target mode */

static struct nfc_digital_ops st25r39_nfc_digital_ops = {
	.in_configure_hw = st25r39_in_configure_hw,
	.in_send_cmd     = st25r39_in_send_cmd,

	.tg_configure_hw = st25r39_tg_configure_hw,
	.tg_get_rf_tech  = st25r39_tg_get_rf_tech,
	.tg_listen       = st25r39_tg_listen,
	.tg_listen_md    = st25r39_tg_listen_md,
	.tg_send_cmd     = st25r39_tg_send_cmd,

	.switch_rf       = st25r39_switch_rf,
	.abort_cmd       = st25r39_abort_cmd,
};

static int st25r39_probe(struct spi_device *spi)
{
	struct device *dev = &spi->dev;
	struct st25r39_data *drvdata;
	int ret;

	if (!spi)
		return -ENODEV;

	drvdata = devm_kzalloc(dev, sizeof(struct st25r39_data), GFP_KERNEL);
	if (!drvdata)
		return -ENOMEM;

	drvdata->config = device_get_match_data(dev);
	if (!(drvdata->config)) {
		dev_err(dev, "failed to find matching device tree id\n");
		ret = -EINVAL;
		goto exit_alloc;
	}

	drvdata->dev = &spi->dev;
	drvdata->spi = spi;

	spi_set_drvdata(spi, drvdata);

	ret = device_property_read_u32(dev, "clock-frequency",
				      &drvdata->clk_freq);
	if (ret) {
		dev_info(
			dev,
		      "Clock frequency not set. Setting to default 27.12 MHz\n");
		drvdata->clk_freq = ST25R39_CLOCK_FREQUENCY_27MHz;
	}

	if (drvdata->clk_freq != ST25R39_CLOCK_FREQUENCY_27MHz &&
	    drvdata->clk_freq != ST25R39_CLOCK_FREQUENCY_13MHz) {
		dev_err(dev, "clock-frequency (%u Hz) unsupported\n",
			drvdata->clk_freq);
		return -EINVAL;
	}

	if (device_property_present(dev, "vdd")) {
		drvdata->supply_vdd = devm_regulator_get(dev, "vdd");
		if (IS_ERR(drvdata->supply_vdd)) {
			dev_err(dev, "error getting regulator vdd\n");
			ret = PTR_ERR(drvdata->supply_vdd);
			goto exit_regulator;
		}

		ret = regulator_enable(drvdata->supply_vdd);
		if (ret) {
			dev_err(dev, "error enabling regulator vdd\n");
			goto exit_regulator;
		}
	}

	if (device_property_present(dev, "vio")) {
		drvdata->supply_vio = devm_regulator_get(dev, "vio");
		if (IS_ERR(drvdata->supply_vio)) {
			dev_err(dev, "error getting regulator vio\n");
			ret = PTR_ERR(drvdata->supply_vio);
			goto exit_regulator;
		}

		ret = regulator_enable(drvdata->supply_vio);
		if (ret) {
			dev_err(dev, "error enabling regulator vio\n");
			goto exit_regulator;
		}
	}

	ret = st25r39_init(drvdata);
	if (ret) {
		dev_err(dev, "Unable to setup ST25R39xx.\n");
		goto exit_regulator;
	}

	ret = devm_request_threaded_irq(dev, spi->irq, NULL, st25r39_irq,
					IRQF_TRIGGER_RISING | IRQF_ONESHOT,
					ST25R39_NAME, drvdata);
	if (ret) {
		dev_err(dev, "Unable to register IRQ handler %d\n", ret);
		goto exit_regulator;
	}

	drvdata->nfc_ddev = nfc_digital_allocate_device(&st25r39_nfc_digital_ops,
	                                                ST25R39_SUPPORTED_PROT,
	                                                ST25R39_CAPABILITIES,
	                                                ST25R39_HEADROOM_LEN,
	                                                ST25R39_TAILROOM_LEN);
	if (!drvdata->nfc_ddev) {
		ret = -ENOMEM;
		goto exit_irq;
	}

	nfc_digital_set_parent_dev(drvdata->nfc_ddev, dev);
	nfc_digital_set_drvdata(drvdata->nfc_ddev, drvdata);
	ret = nfc_digital_register_device(drvdata->nfc_ddev);
	if (ret) {
		nfc_err(dev, "Failed to register digital NFC device\n");
		goto exit_nfc_dev;
	}

	mutex_init(&drvdata->lock);
	sema_init(&drvdata->exchange_lock, 1);
	INIT_DELAYED_WORK(&drvdata->timeout_work, st25r39_timeout_work_handler);

	return 0;

exit_nfc_dev:
	if (drvdata->nfc_ddev)
		nfc_digital_free_device(drvdata->nfc_ddev);
exit_irq:
	if (spi->irq > 0)
		devm_free_irq(dev, spi->irq, drvdata);
exit_regulator:
	if (!IS_ERR_OR_NULL(drvdata->supply_vio))
		regulator_disable(drvdata->supply_vio);
	if (!IS_ERR_OR_NULL(drvdata->supply_vdd))
		regulator_disable(drvdata->supply_vdd);
	if (!IS_ERR_OR_NULL(drvdata->supply_vio))
		devm_regulator_put(drvdata->supply_vio);
	if (!IS_ERR_OR_NULL(drvdata->supply_vdd))
		devm_regulator_put(drvdata->supply_vdd);
exit_alloc:
	if (drvdata)
		devm_kfree(dev, drvdata);

	return ret;
}

static int st25r39_remove(struct spi_device *spi)
{
	int ret;
	struct st25r39_data *drvdata = spi_get_drvdata(spi);
	struct device *dev = &spi->dev;

	dev_info(dev, "%s\n", __func__);

	if (!drvdata)
		return 0;

	if (drvdata->nfc_ddev) {
		nfc_digital_unregister_device(drvdata->nfc_ddev);
		nfc_digital_free_device(drvdata->nfc_ddev);
	}

	st25r39_clear_bits(drvdata, ST25R39_OPERATION_CTL,
	                   ST25R39_OPERATION_CTL_RX_EN |
	                   ST25R39_OPERATION_CTL_TX_EN |
	                   ST25R39_OPERATION_CTL_EN);

	if (drvdata->spi->irq > 0)
		devm_free_irq(dev, drvdata->spi->irq, drvdata);

	if (!IS_ERR_OR_NULL(drvdata->supply_vio)) {
		regulator_disable(drvdata->supply_vio);
		devm_regulator_put(drvdata->supply_vio);
	}
	if (!IS_ERR_OR_NULL(drvdata->supply_vdd)) {
		regulator_disable(drvdata->supply_vdd);
		devm_regulator_put(drvdata->supply_vdd);
	}

	mutex_destroy(&drvdata->lock);

	/* If last in_send_cmd's ISR is pending, wait for it to finish */
	ret = down_killable(&drvdata->exchange_lock);
	if (ret == -EINTR)
		dev_err(dev, "sleep for exchange semaphore interrupted by signal\n");

	devm_kfree(dev, drvdata);

	return 0;
}

static const struct spi_device_id st25r39_id_table[] = {
	{ "st25r3910" },
	{ "st25r3911b" },
	{ "st25r3912" },
	{ "st25r3913" },
	{ "st25r3914" },
	{ "st25r3915" },
	{ "st25r3916" },
	{ "st25r3917" },
	{ /* sentinel */ }
};
MODULE_DEVICE_TABLE(spi, st25r39_id_table);

#ifdef CONFIG_OF
static const struct of_device_id of_st25r39_match[] = {
	{ .compatible = "st,st25r3910", .data = &st25r3910_config, },
	{ .compatible = "st,st25r3911b", .data = &st25r3911b_config, },
	{ .compatible = "st,st25r3912", .data = &st25r3912_config, },
	{ .compatible = "st,st25r3913", .data = &st25r3913_config, },
	{ .compatible = "st,st25r3914", .data = &st25r3914_config, },
	{ .compatible = "st,st25r3915", .data = &st25r3915_config, },
	{ .compatible = "st,st25r3916", .data = &st25r3916_config, },
	{ .compatible = "st,st25r3917", .data = &st25r3917_config, },
	{ /* sentinel */ }
};
MODULE_DEVICE_TABLE(of, of_st25r39_match);
#endif

static struct spi_driver st25r39_driver = {
	.driver = {
		.name = ST25R39_NAME,
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(of_st25r39_match),
	},
	.probe = st25r39_probe,
	.id_table = st25r39_id_table,
	.remove = st25r39_remove,
};

module_spi_driver(st25r39_driver);

MODULE_AUTHOR("Olliver Schinagl <oliver@schinagl.nl>");
MODULE_AUTHOR("Paweł Żabiełowicz <pzabielowicz@onplick.com>");
MODULE_AUTHOR("Yaroslav Palamar <yaroslavpalamar@gmail.com>");
MODULE_DESCRIPTION(ST25R39_DESC);
MODULE_VERSION(ST25R39_VERSION);
MODULE_LICENSE("GPL");
